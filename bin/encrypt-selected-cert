#!/usr/bin/env bash
#
# Encrypt the selected certificate from a given pool.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
source $DIRNAME/../lib/params

# Check
if [ -z "$CONFIG" ]; then
  echo "usage: onionmine $BASENAME <pool>"
  echo ""
  echo "Available pools:"
  echo ""
  ls -1 $POOLS | sed -e 's/^/    /'
  echo ""
  exit 1
elif [ ! -h "$POOL/selected" ]; then
  echo "$BASENAME: please select a candidate first"
  exit 1
elif [ -z "$ENCRYPTION_CERT_COMMAND" ]; then
  echo "$BASENAME: please set the ENCRYPTION_CERT_COMMAND in your config"
  exit 1
fi

# Additional parameters
cd $POOL
ONION_KEYS_PATH="`readlink selected`"
ONION_ADDR="`basename $ONION_KEYS_PATH`"
ONION_CERTS_PATH="$POOL/certs/selected"

# Check for certificates
if [ ! -e "$ONION_CERTS_PATH/privatekey.pem" ]; then
  echo "$BASENAME: please generate certificates first"
  exit 1
fi

# Encrypt
eval $ENCRYPTION_CERT_COMMAND
