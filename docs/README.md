# Onionmine: Onion Services keys and TLS certs generator

![](assets/logo.jpg "Onionmine")

[Onionmine][] is a handy wrapper to:

* Organize the so-called [vanity address][] creation based on the [mkp224o
  generator][].
* Help the TLS certificate generation for Onion Services.

It's useful when one is generating many Onion Services in a row.

[Onionmine]: https://gitlab.torproject.org/tpo/onion-services/onionmine/
[vanity address]: https://community.torproject.org/onion-services/advanced/vanity-addresses/
[mkp224o generator]: https://github.com/cathugger/mkp224o

## Features

* Handy commands to create configuration and mine keys.
* Can be integrated with an external password manager
  to store the random seed.

## Issues

Check the [issue queue](https://gitlab.torproject.org/tpo/onion-services/onionmine/-/issues)
for the current task and issues list.
