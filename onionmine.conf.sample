#
# Sample environment file for Onionmine.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# HINT: You can use any parameter defined at `lib/params`.
#

#
# General behavior
#

# Interactivity. Set for "n" to not ask for user input
ONIONMINE_INTERACTIVE="y"

#
# Default mkp224o flags
#

# Default flags used for mkp224o compilation
MKP224O_BUILD_FLAGS="--enable-intfilter=native --enable-regex=yes"

# Default flags used for mkp224o invocation
MKP224O_RUN_FLAGS="-s -d $CANDIDATES -f $FILTERS -B"

#
# Compilation examples
#

# Enables other filters and compilation parameters
# See https://github.com/cathugger/mkp224o/blob/master/OPTIMISATION.txt for details
#MKP224O_BUILD_FLAGS="--enable-amd64-51-30k --enable-intfilter=native --enable-binsearch --enable-besort"

# Enables regular expression
# See https://github.com/cathugger/mkp224o/issues/5
#MKP224O_BUILD_FLAGS="$MKP224O_BUILD_FLAGS --enable-regex=yes"

#
# Invocation examples
#

# Invokes mkp224o with checkpoint and passphrase
# Passphrase should be provided via the PASSPHRASE environment variable
#MKP224O_RUN_FLAGS="-s --checkpoint $CHECKPOINT -d $CANDIDATES -f $FILTERS -B -P"

# Limit the number of keys per pool at each run
#
# Use the following if you want to limit the number of generated keys each
# time the mkp224o runs. Useful wen running inside a batch job.
#
# When combined with passphrase handling, enables one to easily stop and
# resume key generation in steps and check for acceptable keys before
# continuing the search.
#MKP224O_RUN_FLAGS="$MKP224O_RUN_FLAGS -n 100"

# Use the following if you're satisfied with a single key that matches
# any of the configured filters each time mkp224o runs.
#
# Useful when looking for longer filter (n >= 7 chars) and any match
# is acceptable.
#
# Generated key can then be easily copied to an Onion Service configuration.
#MKP224O_RUN_FLAGS="$MKP224O_RUN_FLAGS -n 1"

#
# Passphrase handling examples
#
# Set a passphrase to initialize the random seed
# Use a random string with at least 64 chars.
#
# You can also fetch the passphrase from an
# external command.
#
# CAUTION: avoid using keys generated with same password for unrelated
# services, as single leaked key may help attacker to regenerate related keys.
#
# Check also https://github.com/cathugger/mkp224o/discussions/95 for the
# security trade-off of this feature.
#

# Store the passphrase in a variable (not recommended)
#PASSPHRASE=""

# Set to 1 to autogenerate a passphrase for each pool
#PASSPHRASE_AUTOGEN="0"

# Fetch the passphrase an external command
#
# Any external command can be used as long as it returns a passphrase in the
# standard output.
#
# Different values of $CONFIG might have different keys.
#
# The following example uses https://0xacab.org/rhatto/keyringer/
#PASSPHRASE_COMMAND="keyringer my-keyring decrypt onionmine/$CONFIG/passphrase 2> /dev/null | head -1"

#
# Remote mining support
#

# Remote hosts
#
# A space-separated list of hostnames.
REMOTE_HOSTS=""

# Whether to sync the code and pools to the remote hosts
REMOTE_HOSTS_RSYNC="0"

# The rsync command to copy the codebase to the remote hosts
# To not include the --delete flag, otherwise your remote pools might be overwritten by a sync
REMOTE_HOSTS_RSYNC_COMMAND="rsync -avz"

# The remote basedir where the pool is located
REMOTE_HOSTS_BASEDIR="/home/onionmine/onionmine"

# Exclude the following patterns from syncing to remote hosts
RSYNC_EXCLUDES="--exclude=candidates --exclude=daemon --exclude=checkpoint.save --exclude=passphrase --exclude=logs"

#
# Other options
#

# Limit the total number of keys per pool
#
# Limit the total number of keys generated in a pool's `candidates/` folder, no
# matter how many times mkp224o is invoked.
#
# Can be combined with mkp224o's "-n" flag, allowing to set bot a per-mining
# sessions and a total keys generated.
#
# Setting to "0" disables the limit.
#MAX_GENERATED_KEYS="0"

# Whether to mine keys only if a selected key does not already exists for a
# given pool.
MINE_ONLY_IF_NO_SELECTED_KEY_EXISTS="1"

# Keyring name used for encryption/decryption
#KEYRING="${KEYRING:-keyring-name}"

# Encryption command
#
# Here you can set an arbitraty command used to encrypt a candidate
# You can use any variable available at runtime, especially the following:
#
# * ONION_ADDR: contains the Onion Service address of the candidate.
# * ONION_KEYS_PATH: the path where the key material is stored.
#
# Make sure to enclose the command in single quotes so the variables are
# evaluated only when the command runs and not when the configuration is
# parsed.
#
# The following example uses https://0xacab.org/rhatto/keyringer/
#ENCRYPTION_COMMAND='keyringer ${KEYRING} encrypt onionmin/${CONFIG}/${ONION_ADDR} ${ONION_KEYS_PATH}'

# Decryption command
#
# Here you can set an arbitraty command used to decrypt a candidate
# You can use any variable available at runtime, especially the following:
#
# * ONION_ADDR: contains the Onion Service address of the candidate.
# * MATERIAL: the Onion Service key material to process, such as
# `hs_ed25519_secret_key` or `hs_ed25519_public_key`.
#
# The following example uses https://0xacab.org/rhatto/keyringer/
#DECRYPTION_COMMAND='keyringer ${KEYRING} decrypt onionmine/${CONFIG}/${ONION_ADDR}/${MATERIAL} > $POOL/candidates/${ONION_ADDR}/${MATERIAL}'

# Encryption command for TLS certificates and keys
#
# Here you can set an arbitraty command used to encrypt certificates
# You can use any variable available at runtime, especially the following:
#
# * ONION_ADDR: contains the Onion Service address of the candidate.
# * ONION_CERTS_PATH: the path where the certificate material is stored.
#
# Make sure to enclose the command in single quotes so the variables are
# evaluated only when the command runs and not when the configuration is
# parsed.
#
# The following example uses https://0xacab.org/rhatto/keyringer/
#ENCRYPTION_CERT_COMMAND='keyringer ${KEYRING} encrypt certs/${CONFIG}/${ONION_ADDR} ${ONION_CERTS_PATH}'

# Decryption command for TLS certificates and keys
#
# Here you can set an arbitraty command used to decrypt certificates
# You can use any variable available at runtime, especially the following:
#
# * ONION_ADDR: contains the Onion Service address of the candidate.
# * MATERIAL: the Onion Service key material to process, such as
# `hs_ed25519_secret_key` or `hs_ed25519_public_key`.
#
# The following example uses https://0xacab.org/rhatto/keyringer/
#DECRYPTION_CERT_COMMAND='keyringer ${KEYRING} decrypt onionmine/${CONFIG}/${ONION_ADDR}/${MATERIAL} > $POOL/certs/selected/${MATERIAL}'

# Whether onion-csr should save it's output
#
# Setting to "1" makes the onion-csr subcommand to save it's output to files
# inside ${POOL}/certs (like the CSR generated and the nonce used).
#
# Setting to "0" disables this feature.
ONION_CSR_SAVE="1"

# How long to wait between each batch task. Specially useful if you want to
# give some interval between keypair generation (generate-selected-cert-batch).
BATCH_WAIT="10"
