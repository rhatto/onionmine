# Onionmine: Onion Services keys and TLS certs generator

Onionmine is a handy wrapper to:

* Organize the so-called [vanity address][] creation based on the [mkp224o
  generator][].
* Help the TLS certificate generation for Onion Services.

It's useful when one is generating many Onion Services in a row.

Full documentation available at the [docs](docs) folder or at [this website][].

[vanity address]: https://community.torproject.org/onion-services/advanced/vanity-addresses/
[mkp224o generator]: https://github.com/cathugger/mkp224o
[this website]: https://onionservices.torproject.org/apps/web/onionmine
